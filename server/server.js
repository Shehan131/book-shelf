const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const config = require('./config/config').get(process.env.NODE_ENV);
const app = express();


mongoose.Promise = global.Promise;
mongoose.connect(config.DATABASE);

const { User } = require('./models/user');
const { Book } = require('./models/book');
const { auth } = require('./middleware/auth');

app.use(bodyParser.json());
app.use(cookieParser());

// GET ONE BOOK//
app.get('/api/getBook',(req,res)=>{
    let bookId = req.query.id;
    Book.findById(bookId,(err, doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(doc)
    })
})

// GET BOOKS//
app.get('/api/getBooks',(req,res)=>{
    let skip = parseInt(req.query.skip);
    let limit = parseInt(req.query.limit);
    let order = req.query.order;

    Book.find().skip(skip).limit(limit).sort({_id:order}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(doc)
    })
})

//GET REVIEWER //
app.get('/api/getReviewer',(req,res)=>{
    let id = req.query.id;

    User.findById(id,(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.json({
            name:doc.name,
            lastname:doc.lastname
        })
    })
})

// GET USERS //
app.get('/api/users',(req,res)=>{
    User.find({},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.json(doc)
    })
})

// GET USER POSTS //
app.get('/api/user_posts',(req,res)=>{
    Book.find({ownerId:req.query.user}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc)
    })
})

// AUTH (need to add auth)//
app.get('/api/auth',(req,res) =>{
    res.json({
        isAuth:true,
        // id:req.user._id,
        // email:req.user.email,
        // name:req.user.name,
        // lastname:req.user.lastname
    })
})

// LOGOUT (need to add auth)//
app.get('/api/logout',(req,res)=>{
    res.sendStatus(200);

})

// POST //
app.post('/api/book',(req,res)=>{
    const book = new Book(req.body);

    book.save((err,doc)=>{
        if(err) return res.status(400).send(err)
        res.status(200).json({
            post:true,
            bookId:doc._id
        })
    })
})

app.post('/api/register',(req,res)=>{
    const user = new User(req.body);

    user.save((err,doc)=>{
        if(err) return res.status(400).json({success:false});
        res.status(200).json({
            success:true,
            user:doc
        })
    })
})



app.post('/api/login',(req,res)=>{

    User.findOne({'email':req.body.email},(err,user)=>{
        if(err) return res.status(400).send(err);
        if(!user) return res.json({isAuth:false,message:'Auth failed, email not found'})

        console.log(user);
        res.json({
            isAuth:true,
            id:user._id,
            email:user.email
        })
      
        user.comparePassword(req.body.password,(err,isMatch)=>{
            if(err) throw res.status(400).send(err);
            if(!isMatch) return res.json({
                isAuth:false,
                message:'Wrong Password'
            });

            
        })
        user.generateToken((err,user)=>{
                if(err) throw res.status(400).send(err);
                res.cookie('auth',user.token).json({
                    isAuth:true,
                    id:user._id,
                    email:user.email
                })
            })
    })
})

// UPDATE //
app.post('/api/updateBook',(req,res)=>{

    Book.findByIdAndUpdate(req.body._id,req.body,{new:true},(err,doc)=>{
        if(err) return res.status(400).send(err)
        res.json({
            success:true,
            doc
        })
    })
})

// DELETE //
app.delete('/api/deleteBook',(req,res)=>{
    Book.findByIdAndRemove(req.query.id,(err,doc)=>{
        if(err) return res.status(400).send(err)
        res.send(true);
    });
})


const port = process.env.PORT || 3001;
app.listen(port,() => {
    console.log('Server Running..');
});

 