# Book-shelf

'Book-shelf' is a simple review management system implemented with MERN stack and Redux.

## Introduction

Users can register then login to the system and add, read, udpate and delete reviews for the books. Purpose of this project is to sharp my skills on MERN stack development with redux.

### Dependencies

React:
	"axios": "^0.17.1",
    "react": "^16.4.1",
    "react-dom": "^16.4.1",
    "react-fontawesome": "^1.6.1",
    "react-redux": "^5.0.6",
    "react-router-dom": "^4.2.2",
    "react-scripts": "1.1.4",
    "react-sidenav": "^2.1.3",
    "react-simple-sidenav": "^0.1.6",
    "redux": "^3.7.2",
    "redux-promise": "^0.5.3",
    "redux-thunk": "^2.2.0"

NodeJS:
	"bcrypt": "^1.0.3",
    "body-parser": "^1.18.2",
    "concurrently": "^3.5.1",
    "cookie-parser": "^1.4.3",
    "express": "^4.16.2",
    "jsonwebtoken": "^8.0.1",
    "moment-js": "^1.1.15",
    "mongoose": "^4.10.8",
    "react-sidenav": "^2.1.3"


## Running the tests

Tests will be written for this project using 'JEST'.

