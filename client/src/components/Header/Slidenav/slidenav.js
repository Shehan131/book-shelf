import React from 'react';
import SideNav from 'react-simple-sidenav';
// import SideNav, { Nav, NavIcon, NavText } from 'react-sidenav';
import {Link} from 'react-router-dom';
import SidenavItems from './sidenavitems';

const MySideNav = (props) => {
    return (
            <SideNav 
                showNav={props.showNav}
                onHideNav={props.onHideNav}
                NavStyle={{
                    background:'#242424',
                    maxWidth:'200px'
                }}
                
                items={[<a to='/user'>Home</a>,'My Profile', 'Add Admins', 'Login', 'Add Reviews', 'My Reviews', 'Logout']}
                >
            
            </SideNav>
    //         <div style={{background: '#2c3e50', color: '#FFF', width: 220}}> 
    //     <SideNav highlightColor='#E91E63' highlightBgColor='#00bcd4' defaultSelected='sales'>       
    //         <Nav id='dashboard'>
    //             <NavIcon></NavIcon>    
    //             <NavText> Dashboard </NavText>
    //         </Nav>
    //         <Nav id='sales'>
    //             <NavIcon>></NavIcon>
    //             <NavText> Sales </NavText>
    //         </Nav>
    //     </SideNav>
    // </div>
        
    );
};

export default MySideNav;