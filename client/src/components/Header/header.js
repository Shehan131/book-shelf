import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import {Link} from 'react-router-dom';

import MySideNav from './Slidenav/slidenav';

class Header extends Component {

    
    state = {
        showNav:false
    }

    onHideNav = () => {
        this.setState({showNav:false})
    }

    render() {

        return (
            <header>
                <div className="open_nav">
                    <FontAwesome name='bars' 
                    style={{
                        color:'#FFFFFF',
                        padding:'10px',
                        cursor:'pointer'
                    }} onClick={()=>this.setState({showNav:true})}/>
                </div>

                <MySideNav showNav={this.state.showNav} onHideNav={()=>this.onHideNav()}> </MySideNav>    

                <Link to='/' className='logo'>
                    The Book Shelf
                </Link>

            </header>
        );
    }
}

export default Header;