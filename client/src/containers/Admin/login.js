import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { loginAction } from '../../actions';

class Login extends Component {

    state = {
        email:'',
        password:'',
        success:false
    }


    handleInputEmail = (event) => {
        this.setState({
            email:event.target.value
        })
    }

    handleInputPassword = (event) => {
        this.setState({
            password:event.target.value
        })
    }

    onSubmit = (event) => {
        event.preventDefault()
        this.props.dispatch(loginAction(this.state))
    }

    render() {
        let user = this.props.user;
        return (
            <div className="rl_container">
                <form onSubmit={this.onSubmit}>
                    <h2>Log in</h2>

                    <div className="form_element">
                        <input onChange={this.handleInputEmail} 
                        type="email" 
                        placeholder="Enter your email"
                        value={this.state.email} 
                        />
                    </div>
                    <div className="form_element">
                        <input 
                        onChange={this.handleInputPassword} 
                        type="password" 
                        placeholder="Enter your password" 
                        value={this.state.password}
                        />
                    </div>
                    <button type="submit">Login</button>
                    <div className="error">
                    {
                        user.login ? user.login.message :null
                    }
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    console.log(state)
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Login)