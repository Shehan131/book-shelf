import axios from 'axios';

export function getBooks(limit,start,order,list){

     const request = axios.get(`/api/getBooks?limit=${limit}&skip=${start}&order=${order}`)
                    .then(response=>{
                        if(list)
                            return [...list,...response.data]
                        else
                            return response.data
                        })
                    .catch(()=>console.log("Book list not available."))
    
                    // 

    // setTimeout(function(){  console.log(request) }, 1000);
   
    
    return{
        type:'GET_BOOKS',
        payload:request
    }

    }



export function getBookWithReviewer(id){
    const request = axios.get(`/api/getBook?id=${id}`)

    return(dispatch)=>{
        request.then(({data})=>{
            let book = data;
           
            axios.get(`/api/getReviewer?id=${book.ownerId}`).then(({data})=>{
                let response = {
                    book,
                    reviewer:data
                }
               
            dispatch({
                type:'GET_BOOK_WITH_RE',
                payload:response
            })
            })
        })
    }
    
}


export function loginAction({email,password}){

    const request = axios.post('/api/login',{email,password})
    .then(response=>response.data)
  
    return {
        type:'USER_LOGIN',
        payload:request
    }
}